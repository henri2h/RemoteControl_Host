﻿using System;
using Microsoft.Win32;
using System.Threading;
using System.IO;

namespace MachineExecutor
{
    class Program
    {
        const int delay = 2000;
        private static ServerConnection connection;
        static bool execute = true;
        static bool connected = false;

        static void Main(string[] args)
        {
            bool regInStart = !File.Exists("disable");

            RegisterInStartup(regInStart);


            while (execute)
            {
                try
                {
                    // connect 
                    // when connected, try every <delay> if we should send commands
                    if (connected == false)
                    {
                        connection = new ServerConnection();
                        connection.Connect();
                        connected = true;
                        Logger.logMain("Connected");
                    }
                    else
                    {
                        connection.GetCommands();
                    }
                }
                catch
                {
                    System.Diagnostics.Debug.WriteLine("Could not connect, trying in " + delay + "ms");
                }

                Thread.Sleep(delay);
            }
            Logger.logMain("Press q key to stop ...");
            Console.ReadLine();
        }

        private static void RegisterInStartup(bool isChecked)
        {
            string appName = "MachineExecutor";
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey
                    ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (isChecked)
            {
                registryKey.SetValue(appName, System.Reflection.Assembly.GetExecutingAssembly().Location);
                Logger.logMain("Added program to registry for auto startup");
            }
            else
            {
                registryKey.DeleteValue(appName);
                Logger.logMain("Deleted program from registry for auto startup");
            }
        }


       
    }

    public class Logger
    {
        public static void logMain(string text)
        {
            System.Diagnostics.Debug.WriteLine("[ Machine controller ] : " + text);
        }
    }
}
