﻿using MachineExecutor.Control;
using MachineExecutor.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineExecutor
{
    public class ServerConnection
    {
        HttpClientHelper client;
        SessionController sessionController;
        public ServerConnection()
        {
            client = new HttpClientHelper("Http://henri2h.fr:8070");

            // initialize the session controller
            sessionController = new SessionController();
        }

        public void Connect()
        {
            System.Diagnostics.Debug.WriteLine(Environment.MachineName);
            client.Connect(Environment.MachineName);
        }
        public void SendInformations() { }

        public void GetCommands()
        {
            List<CommandObject> commands = client.GetCommands();
            foreach (CommandObject command in commands)
            {
                try
                {
                    InterpretCommands(command);
                }
                catch (Exception ex)
                {
                    command.Result = ex.Message;
                }

                System.Diagnostics.Debug.WriteLine("Result : " + command.Result);
                client.SetCommandResult(command);
            }
        }

        public void InterpretCommands(CommandObject command)
        {
            string cresult = "ERROR";
            switch (command.Command) // TODO : add JSON here
            {
                case "clientType":
                    cresult = "machine";
                    System.Diagnostics.Debug.WriteLine("Connected");
                    break;

                case "listSession":
                    string jsonSession = Newtonsoft.Json.JsonConvert.SerializeObject(sessionController.ListSession());
                    cresult = jsonSession;
                    break;

                case "lockSession":
                    sessionController.LockWorkStation();
                    break;

                case "listProcess":
                    string jsonSessionprocess = Newtonsoft.Json.JsonConvert.SerializeObject(ProcessManager.GetProcesses());
                    cresult = jsonSessionprocess;
                    break;

                case "killProcess": // TODO : get the name of the process
                    string procName = "";
                    int procID = Int32.Parse(procName);
                    ProcessManager.KillProcess(procID);
                    break;

                case "createProcess":
                    string processName = "cmd";
                    bool result = ProcessManager.StartProcess(processName);
                    cresult = result.ToString();
                    break;

                case "getMachineName":
                    cresult = Environment.MachineName;
                    break;

                case "listFiles":
                    string path = "";
                    List<DirectoryObject> files = Files.ListDirectory(path);
                    cresult = Newtonsoft.Json.JsonConvert.SerializeObject(files);
                    break;
                /*case "readFile":
                    client.writeLine(FileManagement.ReadFile(client.readLine()));
                    break;
                case "writeFile":
                    result = FileManagement.WriteFile(client.readLine());
                    break;
                case "readFileBytes":
                    client.writeLine(FileManagement.ReadFileBytes(client.readLine()));
                    break;
                case "writeFileBytes":
                    result = FileManagement.WriteFileBytes(client.readLine());
                    break;
                    */
                default:
                    System.Diagnostics.Debug.WriteLine("Command : <" + command.Command + "> not recognized");
                    break;
            }

            command.Result = cresult;
        }
    }
}
