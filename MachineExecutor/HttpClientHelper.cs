﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MachineExecutor
{
    public class HttpClientHelper
    {
        private string _machineID;

        private RestClient _client;

        public HttpClientHelper(string url)
        {
            _client = new RestClient(url);
        }


        public string GetRequest(string url)
        {
            var request = new RestRequest("/add_devices", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Accept", "application/json");
            var body = new
            {
                name = "namedevice"
            };
            request.AddJsonBody(body);
            IRestResponse response = _client.Execute(request);
            System.Diagnostics.Debug.WriteLine(response.Content);
            return response.Content;
        }



        public string Connect(string machineName)
        {
            var request = new RestRequest("/add_devices", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Accept", "application/json");
            var body = new
            {
                name = machineName
            };
            request.AddJsonBody(body);
            IRestResponse response = _client.Execute(request);

            // set machine id
            // TODO : add a better login mechanism to setup a token and an id
            _machineID = machineName;


            JObject o = JObject.Parse(response.Content);
            if ((string)o["message"] == "success")
            {
                _machineID = (string)o["data"]["name"];
                System.Diagnostics.Debug.WriteLine("Success : " + _machineID);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error : " + (string)o["error"]);
            }
            return response.Content;
        }


        public List<CommandObject> GetCommands()
        {
            var request = new RestRequest("/get_commands", Method.POST)
            {
                RequestFormat = DataFormat.Json
            };
            request.AddHeader("Accept", "application/json");
            var body = new
            {
                id = _machineID // next we should change to another approche for id (random UUID)
            };
            request.AddJsonBody(body);
            IRestResponse response = _client.Execute(request);

            JObject o = JObject.Parse(response.Content);
            if ((string)o["message"] == "success")
            {
                System.Diagnostics.Debug.WriteLine("Commands :");
                List<CommandObject> commands = new List<CommandObject>(); 
                foreach (var item in o["data"])
                {
                    CommandObject command = new CommandObject();
                    command.Command = (string)item["command"];
                    command.Command_id = (int)item["command_id"];
                    commands.Add(command);
                }
                return commands;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error : " + (string)o["error"]);
            }
            return null;
        }


        public string SetCommandResult(CommandObject command)
        {
            var request = new RestRequest("/set_command_result", Method.POST)
            {
                RequestFormat = DataFormat.Json
            };
            request.AddHeader("Accept", "application/json");
            var body = new
            {
                id = _machineID,
                command_id = command.Command_id,
                result = command.Result
            };
            request.AddJsonBody(body);
            IRestResponse response = _client.Execute(request);


            JObject o = JObject.Parse(response.Content);
            if ((string)o["message"] == "success")
            {
                System.Diagnostics.Debug.WriteLine("Success");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error : " + (string)o["error"]);
            }
            return response.Content;
        }

    }
}
