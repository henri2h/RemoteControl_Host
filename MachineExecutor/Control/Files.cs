﻿using MachineExecutor.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineExecutor.Control
{
    public class Files
    {
        public static List<DirectoryObject> ListDirectory(string path)
        {
            List<DirectoryObject> files = new List<DirectoryObject>();

            if (path == "" || Directory.Exists(path) == false)
            {
                path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            }


            foreach (string file in Directory.EnumerateFileSystemEntries(path))
            {
                DirectoryObject newFile = new DirectoryObject();

                FileAttributes attr = File.GetAttributes(file);
                if (attr.HasFlag(FileAttributes.Directory))
                {
                    newFile.IsDirectory = true;
                }
                else
                {
                    newFile.IsDirectory = false;
                }

                newFile.Name = Path.GetFileName(file);

                newFile.LastAccess = File.GetLastAccessTime(file);
                newFile.Path = file;


                files.Add(newFile);
            }
            return files;
        }

        public static string ReadFile(string path)
        {
            string text = File.ReadAllText(path);
            return text;
        }
        public static void WriteFile(string path, string text)
        {
            File.WriteAllText(path, text);
        }
    }
}
