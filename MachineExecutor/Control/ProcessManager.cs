﻿using MachineExecutor.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineExecutor.Control
{
    public class ProcessManager
    {
        public static ApplicationObject[] GetProcesses()
        {
            List<ApplicationObject> apps = new List<ApplicationObject>();
            foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses("."))
            {
                ApplicationObject app = new ApplicationObject();
                app.Id = process.Id;
                app.ProcessName = process.ProcessName;
                app.WindowHandle = process.MainWindowHandle.ToString();
                app.WindowTitle = process.MainWindowTitle;
                app.MemoryAllocation = process.PrivateMemorySize64.ToString();

                apps.Add(app);
            }
            return apps.ToArray();
        }

        /// <summary>
        /// Kill a process based on his name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool KillProcess(string name)
        {
            try
            {
                foreach (System.Diagnostics.Process proc in System.Diagnostics.Process.GetProcessesByName(name))
                {
                    proc.Kill();
                }
                return true;
            }
            catch (Exception ex)
            {
                ex.Source = "MachineManager.Process.ProcessManager.killProcess";

                //ErrorHandeler.Manager.printOut(ex);
                return false;
            }
        }

        /// <summary>
        /// Kill a process based on his ID
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static bool KillProcess(int Id)
        {
            try
            {
                System.Diagnostics.Process.GetProcessById(Id).Kill();
                return true;
            }
            catch (Exception ex)
            {
                ex.Source = "MachineManager.Process.ProcessManager.killProcess";
               // ErrorHandeler.Manager.printOut(ex);
                return false;
            }
        }

        /// <summary>
        /// Start a process based on his name
        /// </summary>
        /// <param name="name">Process name</param>
        /// <returns></returns>
        public static bool StartProcess(string name)
        {
            try
            {
                System.Diagnostics.Process proc = System.Diagnostics.Process.Start(name);

            }
            catch (Exception ex)
            {
               // ErrorHandeler.Manager.printOut(ex);
                return false;
            }
            return true;
        }
    }
}
