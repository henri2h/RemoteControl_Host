﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineExecutor.Control
{
    public class FileManager
    {
        public static String ReadFile(String input)
        {
            string path = input;
            JObject rt = new JObject();

            if (File.Exists(path))
            {
                rt.Add("success", true);
                rt.Add("text", File.ReadAllText(path));
            }
            else
            {
                rt.Add("success", false);
            }
            return rt.ToString();
        }

        public static String WriteFile(String input)
        {
            JObject o = JObject.Parse(input);
            String path = (String)o["path"];
            String content = (String)o["text"];
            File.WriteAllText(path, content);
            JObject resp = new JObject
            {
                { "success", true }
            };
            return resp.ToString();
        }

        public static String ReadFileBytes(String input)
        {
            JObject o = JObject.Parse(input);
            String path = (String)o["path"];
            JObject rt = new JObject();

            if (File.Exists(path))
            {
                rt.Add("success", true);
                rt.Add("text", File.ReadAllBytes(path));
            }
            else
            {
                rt.Add("success", false);
            }
            return rt.ToString();
        }

        public static String WriteFileBytes(String input)
        {
            JObject o = JObject.Parse(input);
            String path = (String)o["path"];
            Byte[] content = (Byte[])o["text"];
            File.WriteAllBytes(path, content);

            JObject resp = new JObject
            {
                { "success", true }
            };
            return resp.ToString();
        }
    }
}
