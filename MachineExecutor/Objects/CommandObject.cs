﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineExecutor
{
    public class CommandObject
    {
        public string Command { get; set; }
        public int Command_id { get; set; }
        public string Result { get; set; }
    }
}
