﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineExecutor.Objects
{
    public class ApplicationObject
    {
        public int Id;
        public string WindowTitle;
        public string WindowHandle;

        public string ProcessName;
        public string MemoryAllocation;
    }
}
